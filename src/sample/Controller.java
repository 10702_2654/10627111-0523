package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button btnworking;
    public ProgressBar pbworking;
    public ImageView iv;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //pbworking.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        pbworking.setVisible(false);
    }

    public void onworking(ActionEvent actionEvent) {
        Task task = new Task<Void>() {


            @Override
            protected Void call() throws Exception {

                final int max = 100;
                for (int i = 1; i <= max; i++) {
                    Thread.sleep(100);
                    updateProgress(i, max);
                    }

                return null;
            }

            @Override
            protected void scheduled() {
                super.scheduled();
                pbworking.setVisible(true);
                iv.setVisible(true);
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                pbworking.setVisible(false);
                iv.setVisible(false);
            }

            @Override
            protected void failed() {
                super.failed();

            }
        };

        //pbworking.progressProperty().bind(task.progressProperty());
        pbworking.setVisible(true);
        new Thread(task).start();
        List<String> image = new ArrayList<>();
        for(int i=0;i<5;i++)
        {
            image.add("/image/".concat(String.valueOf(i)).concat(".jpg"));
        }
        Timeline tl =new Timeline();
        tl.setCycleCount(Timeline.INDEFINITE);
        tl.setAutoReverse(false);

        for(int i=0;i<5;i++)
        {
            int j=i;
            tl.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            iv.setImage(new Image(getClass().getResource(image.get(j)).toString()));
                        }
                    })
            );
        }
        tl.play();
    }
}
